FROM gcc
MAINTAINER Tharusha Atukorala
RUN apt-get update && apt-get install -y \
vim \
gdb \
git
RUN git config --global user.email "atukoralat@gmail.com"
RUN git config --global user.name "Tharusha420"
